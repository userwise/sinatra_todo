require 'sinatra'
require 'sinatra/activerecord'
require './user'
require './list'
require 'pry'

set :database, "sqlite3:todoapp.sqlite3"

get "/users" do 
  # pull all users from db
  @users = User.all
  erb :users
end

get "/users/:id" do 
  @user = User.find_by_id(params[:id])
  erb :user_detail
end